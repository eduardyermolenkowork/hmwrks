const servicesContent = {
    webDesign: {
        img: "img/service-img1.png",
        text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    },
    graphicDesign: {
        img: "img/service-img2.jpg",
        text: "Graphic design is the process of visual communication and problem-solving through the use of typography, photography and illustration. The field is considered a subset of visual communication and communication design, but sometimes the term \"graphic design\" is used synonymously. Graphic designers create and combine symbols, images and text to form visual representations of ideas and messages. They use typography, visual arts and page layout techniques to create visual compositions. Common uses of graphic design include corporate design (logos and branding), editorial design (magazines, newspapers and books), wayfinding or environmental design, advertising, web design, communication design, product packaging and signage."
    },
    onlineSupport: {
        img: "img/service-img3.jpg",
        text: "Customer support is a range of customer services to assist customers in making cost effective and correct use of a product. It includes assistance in planning, installation, training, troubleshooting, maintenance, upgrading, and disposal of a product. Regarding technology products such as mobile phones, televisions, computers, software products or other electronic or mechanical goods, it is termed technical support."
    },
    appDesign: {
        img: "img/service-img4.jpg",
        text: "Application software (app for short) is software designed to perform a group of coordinated functions, tasks, or activities for the benefit of the user. Examples of an application include a word processor, a spreadsheet, an accounting application, a web browser, an email client,a media player, a file viewer, an aeronautical flight simulator, a console game or a photo editor. The collective noun application software refers to all applications collectively. This contrasts with system software, which is mainly involved with running the computer."
    },
    onlineMarketing: {
        img: "img/service-img5.jpg",
        text:"Digital marketing is the marketing of products or services using digital technologies, mainly on the Internet, but also including mobile phones, display advertising, and any other digital medium. Digital marketing's development since the 1990s and 2000s has changed the way brands and businesses use technology for marketing. As digital platforms are increasingly incorporated into marketing plans and everyday life, and as people use digital devices instead of visiting physical shops, digital marketing campaigns are becoming more prevalent and efficient."
    },
    seoService: {
        img: "img/service-img6.jpg",
        text: "Search engine optimization (SEO) is the process of affecting the online visibility of a website or a web page in a web search engine's unpaid results—often referred to as \"natural\", \"organic\", or \"earned\" results. In general, the earlier (or higher ranked on the search results page), and more frequently a website appears in the search results list, the more visitors it will receive from the search engine's users; these visitors can then be converted into customers. SEO may target different kinds of search, including image search, video search, academic search, news search, and industry-specific vertical search engines."
    }
};
const buttons = document.getElementsByClassName("services-btn");
let currentService = buttons[0];
let imgHTML = document.getElementById("service-img");
let textHTML = document.getElementById("services-text");
[].forEach.call(buttons, (elem) => {
    elem.addEventListener("click", () => {
        currentService.classList.remove("services-btn-active");
        currentService = event.currentTarget;
        currentService.classList.add("services-btn-active");
        imgHTML.src = servicesContent[currentService.id.substring(4, currentService.id.length)].img;
        textHTML.textContent = servicesContent[currentService.id.substring(4, currentService.id.length)].text;
    })
});