const imagesStorage = {
    graphicDesign: [
        "img/work_img/graphic_design/graphic-design1.jpg",
        "img/work_img/graphic_design/graphic-design2.jpg",
        "img/work_img/graphic_design/graphic-design3.jpg",
        "img/work_img/graphic_design/graphic-design4.jpg",
        "img/work_img/graphic_design/graphic-design5.jpg",
        "img/work_img/graphic_design/graphic-design6.jpg",
        "img/work_img/graphic_design/graphic-design7.jpg",
        "img/work_img/graphic_design/graphic-design8.jpg",
        "img/work_img/graphic_design/graphic-design9.jpg",
        "img/work_img/graphic_design/graphic-design10.jpg",
        "img/work_img/graphic_design/graphic-design11.jpg",
        "img/work_img/graphic_design/graphic-design12.jpg"
    ],
    landingPages: [
        "img/work_img/landing_page/landing-page1.jpg",
        "img/work_img/landing_page/landing-page2.jpg",
        "img/work_img/landing_page/landing-page3.jpg",
        "img/work_img/landing_page/landing-page4.jpg",
        "img/work_img/landing_page/landing-page5.jpg",
        "img/work_img/landing_page/landing-page6.jpg",
        "img/work_img/landing_page/landing-page7.jpg",
    ],
    webDesign: [
        "img/work_img/web_design/web-design1.jpg",
        "img/work_img/web_design/web-design2.jpg",
        "img/work_img/web_design/web-design3.jpg",
        "img/work_img/web_design/web-design4.jpg",
        "img/work_img/web_design/web-design5.jpg",
        "img/work_img/web_design/web-design6.jpg",
        "img/work_img/web_design/web-design7.jpg",
    ],
    wordpress: [
        "img/work_img/wordpress/wordpress1.jpg",
        "img/work_img/wordpress/wordpress2.jpg",
        "img/work_img/wordpress/wordpress3.jpg",
        "img/work_img/wordpress/wordpress4.jpg",
        "img/work_img/wordpress/wordpress5.jpg",
        "img/work_img/wordpress/wordpress6.jpg",
        "img/work_img/wordpress/wordpress7.jpg",
        "img/work_img/wordpress/wordpress8.jpg",
        "img/work_img/wordpress/wordpress9.jpg",
        "img/work_img/wordpress/wordpress10.jpg"
    ],
    getArrImg: () => {
       let arrVal = Object.values(imagesStorage);
       let arrImg = arrVal.filter(arr =>
            Array.isArray(arr)
        );
       let newArrImg = [];
       for (let i = 0; i < arrImg.length ; i++) {
           newArrImg = newArrImg.concat(arrImg[i]);
       }
       return newArrImg;
    },
    getArrRandomImg: () => {
        let newArr = [];
        let oldArr = imagesStorage.getArrImg();
        while (oldArr.length > 0) {
            newArr.push(oldArr.splice((Math.random()*(oldArr.length-1))>>0, 1)[0])
        }
        return newArr;
    }
};
let beginIndex = 0;
const allRandomImages = imagesStorage.getArrRandomImg();
let wrapperImg = document.getElementById("work-img-wrapper");
let wrapperMain = document.getElementById("work-content-wrapper");
/******************************************
  create a hover-block
 ******************************************/
const createHoverBlock = () => {
    const hoverBlock = document.createElement("div");
    hoverBlock.classList.add("work-container-hover");
    let linkWrapper = document.createElement("div");
    linkWrapper.classList.add("work-container-hover-link-wrapper");
    let Link_1 = document.createElement("a");
    let Link_2 = document.createElement("a");
    Link_1.innerHTML = "<i class=\"fas fa-link work-container-icon\"></i>";
    Link_2.innerHTML = "<i class=\"fas fa-search work-container-icon wci-white\"></i>";
    linkWrapper.appendChild(Link_1);
    linkWrapper.appendChild(Link_2);
    let Text_1 = document.createElement("p");
    Text_1.textContent = "creative design";
    let Text_2 = document.createElement("p");
    Text_2.textContent = "Web Design";
    hoverBlock.appendChild(linkWrapper);
    hoverBlock.appendChild(Text_1);
    hoverBlock.appendChild(Text_2);
    return hoverBlock;
};

/******************************************
 create a image-box with hover-block
 ******************************************/
const createImageBox = (storage) => {
    let img = document.createElement("img");
    let wrapperDiv = document.createElement("div");
    img.src = storage;
    img.addEventListener("mouseover", ()=>{
            let contentBox = createHoverBlock();
            contentBox.addEventListener("mouseleave", ()=>{
                contentBox.addEventListener("transitionend", ()=>{
                    event.currentTarget.remove();
                });
            });
        event.currentTarget.parentElement.appendChild(contentBox);
    });
    wrapperDiv.classList.add("work-single-img-wrapper");
    wrapperDiv.appendChild(img);
    return wrapperDiv;
};
/**************************
 declarate a functions of append images blocks
 **************************/
const appendImg = (key, parent) => {
    let docFr = document.createDocumentFragment();
    for (let i = 0; i < imagesStorage[key].length; i++) {
        docFr.appendChild(createImageBox(imagesStorage[key][i]));
    }
    parent.appendChild(docFr);
};
const appendAllImg = (count, parent) => {
    let docFr = document.createDocumentFragment();
    for (beginIndex; beginIndex < count; beginIndex++) {
        docFr.appendChild(createImageBox(allRandomImages[beginIndex]));
    }
    parent.appendChild(docFr);
};
let buttonAppend = document.getElementById("btn-append-all");
buttonAppend.addEventListener("click", () => {
    appendAllImg(beginIndex + 12, wrapperImg);
    if (wrapperImg.children.length === allRandomImages.length) {
        buttonAppend.remove();
    }
});
let buttonsWork = document.getElementsByClassName("work-menu-btn");
let currentActive = buttonsWork[0];
[].forEach.call(buttonsWork, (elem)=>{
    elem.addEventListener("click", ()=>{
        currentActive.classList.remove("work-menu-btn-active");
        currentActive = event.currentTarget;
        currentActive.classList.add("work-menu-btn-active");
        wrapperImg.innerHTML = null;
        let key = event.currentTarget.id.substring(7, event.currentTarget.id.length);
        if (event.currentTarget.id !== "wm-btn-all"){
            appendImg(key, wrapperImg);
            if(document.getElementById("btn-append-all") !== null) {
                wrapperMain.removeChild(buttonAppend);
            }
        } else {
            beginIndex = 0;
            appendAllImg(12, wrapperImg);
            if(document.getElementById("btn-append-all") == null) {
                wrapperMain.appendChild(buttonAppend);
            }
        }
    })
});
buttonsWork[0].click();