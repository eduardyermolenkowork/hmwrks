const commentsStorage = [
    {
        photo: "img/people_img1.jpeg",
        comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur",
        name: "Alan Butcher",
        position: "Back-end Developer"
    },
    {
        photo: "img/people_img2.jpeg",
        comment: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
        name: "Bill Walker",
        position: "UI/UX Designer"
    },
    {
        photo: "img/people_img3.jpeg",
        comment: "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        name: "Sam Jonson",
        position: "Front-end Developer"
    },
    {
        photo: "img/people_img4.jpeg",
        comment: "Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
        name: "John Carter",
        position: "Senior JS Developer"
    }
];
const getNumOfItem = (item) =>{
    return Number(item.id.substring(5, item.id.length))
};
let comment = document.getElementById("comment");
let slaider = document.getElementById("slaider-comments");
commentsStorage.forEach((elem, index)=>{
    let item = document.createElement("div");
    item.classList.add("slaider-item-wrapper");
    let linkItem = document.createElement("a");
    linkItem.classList.add("slaider-item");
    let img = document.createElement("img");
    img.src = elem.photo;
    linkItem.appendChild(img);
    linkItem.id = `item-${index}`;
    item.appendChild(linkItem);
    slaider.appendChild(item);
});

let itemsSlaider = document.getElementsByClassName("slaider-item");
let imageWrapper = document.getElementById("image-wrapper");
let currentItem = itemsSlaider[0];
[].forEach.call(itemsSlaider, (elem)=>{
    elem.addEventListener("click", ()=>{
        currentItem.classList.remove("slaider-item-active");
        currentItem = event.currentTarget;
        currentItem.classList.add("slaider-item-active");
        comment.textContent = commentsStorage[getNumOfItem(currentItem)].comment;
        document.getElementById("commentator-name").textContent = commentsStorage[getNumOfItem(currentItem)].name;
        document.getElementById("commentator-pos").textContent = commentsStorage[getNumOfItem(currentItem)].position;
        imageWrapper.innerHTML = null;
        let topImg = document.createElement("img");
        topImg.src = commentsStorage[getNumOfItem(currentItem)].photo;
        imageWrapper.appendChild(topImg);

    })
});
const buttonRight = document.getElementById("btn-right");
buttonRight.addEventListener("click", () => {
    if(getNumOfItem(currentItem) < itemsSlaider.length - 1){
        itemsSlaider[getNumOfItem(currentItem) + 1].click();
    }
});
const buttonLeft = document.getElementById("btn-left");
buttonLeft.addEventListener("click", () => {
    if(getNumOfItem(currentItem) > 0){
        itemsSlaider[getNumOfItem(currentItem) - 1].click();
    }
});
itemsSlaider[0].click();